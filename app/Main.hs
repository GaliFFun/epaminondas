module Main where

import Api
import Control.Concurrent.STM.TVar (newTVar)
import Control.Monad.STM           (atomically)
import Data.Map
import Network.Wai.Handler.Warp

main :: IO ()
--main = run 8080 $ app $ State (atomically $ newTVar (empty))

main = do
  initialMap <- atomically $ newTVar (empty)
  run 8080 $ app $ State initialMap
