import Test.Tasty
import Test.Tasty.HUnit
import qualified Test.Tasty.QuickCheck as QC
import qualified Test.Tasty.SmallCheck as SC

import Test.SmallCheck.Series

import Types
import Lib

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [libUnitTests, libSmallCheckTests, libQuickCheckTests, typesUnitTests, typesSmallCheckTests, typesQuickCheckTests]

-- Тесты для вспомогательных фукнций
libUnitTests :: TestTree
libUnitTests = testGroup "Lib Unit tests" [
  testCase "mergePairs" $ mergePairs (+) (1, 1) (1, 1) @?= (2, 2),
  testCase "replace in empty list" $ replace (\x -> x ^ 2) 3 [] @?= [],
  testCase "replace" $ replace (\x -> x ^ 2) 2 [0, 1, 2, 3] @?= [0, 1, 4, 3],
  testCase "replace2D in empty list of lists" $ replace2D (\x -> x ^ 2) (1, 1) [[]] @?= [[]],
  testCase "replace2D" $ replace2D (\x -> x ^ 2) (1, 1) [[1, 2], [3, 4]] @?= [[1, 2], [3, 16]], 
  testCase "replaceWithDefault (Just)" $ replaceWithDefault (Just 1) 2 @?= Just 1,
  testCase "replaceWithDefault (Nothing)" $ replaceWithDefault Nothing 2 @?= Just 2,
  testCase "between (success)" $ between 2 1 3 @?= True,
  testCase "between (fail)" $ between 1 3 4 @?= False ]

libSmallCheckTests :: TestTree
libSmallCheckTests =
  -- localOption (QuickCheckVerbose True) $
  testGroup "Lib SmallCheck tests"
  [ SC.testProperty "mergePairs" $ \(a, b) (c, d) -> (mergePairs (max::(Int -> Int -> Int)) (a, b) (c, d)) == (max a c, max b d), 
    SC.testProperty "replaceWithDefault (Just)" $ \a b -> ((replaceWithDefault::(Maybe Int -> Int -> Maybe Int)) (Just a) b) == Just a,
    SC.testProperty "replaceWithDefault (Nothing)" $ \x -> ((replaceWithDefault::(Maybe Int -> Int -> Maybe Int)) (Nothing) x) == Just x,
    SC.testProperty "replace (length)" $ \list index -> (index >= 0 && index < (length::([Int] -> Int)) list) SC.==> (((length::([Int] -> Int)) $ replace (id) index list) == (length::([Int] -> Int)) list),
    SC.testProperty "replace (content)" $ \list index -> (index >= 0 && index < (length::([Int] -> Int)) list) SC.==> ((replace (+1) index list) !! index) == (list !! index) + 1,
    SC.testProperty "getOrElse (Just)" $ \x y -> ((getOrElse::(Maybe Int -> Int -> Int)) (Just x) y) == x,
    SC.testProperty "getOrElse (Nothing)" $ \y -> ((getOrElse::(Maybe Int -> Int -> Int)) (Nothing) y) == y ]

libQuickCheckTests :: TestTree
libQuickCheckTests =
  -- localOption (QuickCheckVerbose True) $
  testGroup "Lib QuickCheck tests"
  [ QC.testProperty "mergePairs" $ \(a, b) (c, d) -> (mergePairs (max::(Int -> Int -> Int)) (a, b) (c, d)) == (max a c, max b d),
    QC.testProperty "replaceWithDefault (Just)" $ \a b -> ((replaceWithDefault::(Maybe Int -> Int -> Maybe Int)) (Just a) b) == Just a,
    QC.testProperty "replaceWithDefault (Nothing)" $ \x -> ((replaceWithDefault::(Maybe Int -> Int -> Maybe Int)) (Nothing) x) == Just x,
    QC.testProperty "replace (length)" $ \list index -> (index >= 0 && index < (length::([Int] -> Int)) list) QC.==> (((length::([Int] -> Int)) $ replace (id) index list) == (length::([Int] -> Int)) list),
    QC.testProperty "replace (content)" $ \list index -> (index >= 0 && index < (length::([Int] -> Int)) list) QC.==> ((replace (+1) index list) !! index) == (list !! index) + 1,
    QC.testProperty "getOrElse (Just)" $ \x y -> ((getOrElse::(Maybe Int -> Int -> Int)) (Just x) y) == x,
    QC.testProperty "getOrElse (Nothing)" $ \y -> ((getOrElse::(Maybe Int -> Int -> Int)) (Nothing) y) == y ]


-- Тесты для типов

-- Доска с нечетным числом строк
testOddBoard :: Board
testOddBoard = Board [
  [(Just $ PlayerPiece White (0, 0)), (Just $ PlayerPiece White (0, 1)), (Just $ PlayerPiece White (0, 2))],
  [Nothing, Nothing, Nothing], 
  [(Just $ PlayerPiece Black (2, 0)), (Just $ PlayerPiece Black (2, 1)), (Just $ PlayerPiece Black (2, 2))] ]

-- Доска с четным числом строк
testEvenBoard :: Board
testEvenBoard = Board [
  [(Just $ PlayerPiece White (0, 0)), (Just $ PlayerPiece White (0, 1)), (Just $ PlayerPiece White (0, 2))],
  [Nothing, Nothing, Nothing], 
  [Nothing, Nothing, Nothing],
  [(Just $ PlayerPiece Black (3, 0)), (Just $ PlayerPiece Black (3, 1)), (Just $ PlayerPiece Black (3, 2))] ]

-- Доска с четным числом строк после перемещения фишки
testEvenBoard' :: Board
testEvenBoard' = Board [
  [Nothing, (Just $ PlayerPiece White (0, 1)), (Just $ PlayerPiece White (0, 2))],
  [(Just $ PlayerPiece White (1, 0)), Nothing, Nothing], 
  [Nothing, Nothing, Nothing],
  [(Just $ PlayerPiece Black (3, 0)), (Just $ PlayerPiece Black (3, 1)), (Just $ PlayerPiece Black (3, 2))] ]

-- Выигрышная ситуация черных
blackWonBoard :: Board 
blackWonBoard = Board [
  [(Just $ PlayerPiece Black (0, 0)), (Just $ PlayerPiece Black (0, 1)), (Just $ PlayerPiece Black (0, 2))],
  [(Just $ PlayerPiece White (1, 0)), Nothing, Nothing],
  [(Just $ PlayerPiece White (2, 0)), Nothing, (Just $ PlayerPiece White (2, 2))] ]

-- Выигрышная ситуация белых
whiteWonBoard :: Board 
whiteWonBoard = Board [
  [(Just $ PlayerPiece Black (0, 0)), Nothing, (Just $ PlayerPiece Black (0, 2))],
  [Nothing, (Just $ PlayerPiece Black (1, 1)), Nothing],
  [(Just $ PlayerPiece White (2, 0)), (Just $ PlayerPiece White (2, 1)), (Just $ PlayerPiece White (2, 2))] ]

-- Доска для проверки перемещения фаланги
phalanxBoard :: Board
phalanxBoard = Board [
  [Nothing, Nothing, (Just $ PlayerPiece White (0, 2)), (Just $ PlayerPiece White (0, 3)), Nothing],
  [Nothing, Nothing, Nothing, Nothing, Nothing], 
  [(Just $ PlayerPiece White (2, 0)), (Just $ PlayerPiece White (2, 1)), (Just $ PlayerPiece White (2, 2)), (Just $ PlayerPiece Black (2, 3)), (Just $ PlayerPiece Black (2, 4))],
  [(Just $ PlayerPiece Black (3, 0)), (Just $ PlayerPiece Black (3, 1)), Nothing, Nothing, Nothing] ]

-- Доска после перемещения фаланги
phalanxBoard' :: Board
phalanxBoard' = Board [
  [(Just $ PlayerPiece White (0, 0)), (Just $ PlayerPiece White (0, 1)), Nothing, Nothing, Nothing],
  [Nothing, Nothing, Nothing, Nothing, Nothing], 
  [(Just $ PlayerPiece White (2, 0)), (Just $ PlayerPiece White (2, 1)), (Just $ PlayerPiece White (2, 2)), (Just $ PlayerPiece Black (2, 3)), (Just $ PlayerPiece Black (2, 4))],
  [(Just $ PlayerPiece Black (3, 0)), (Just $ PlayerPiece Black (3, 1)), Nothing, Nothing, Nothing] ]

-- Доска для проверки захвата с перемещением фаланги 
captureBoard :: Board
captureBoard = Board [
  [Nothing, Nothing, (Just $ PlayerPiece White (0, 2)), (Just $ PlayerPiece White (0, 3)), Nothing],
  [Nothing, Nothing, Nothing, Nothing, Nothing], 
  [Nothing, (Just $ PlayerPiece White (2, 1)), (Just $ PlayerPiece White (2, 2)), (Just $ PlayerPiece White (2, 3)), Nothing],
  [(Just $ PlayerPiece Black (3, 0)), (Just $ PlayerPiece Black (3, 1)), Nothing, Nothing, Nothing] ]

-- Доска для проверки зеркальности при передвижении фаланги
mirrorTestBoard :: Board
mirrorTestBoard = Board [
  [(Just $ PlayerPiece White (0, 0)), (Just $ PlayerPiece White (0, 1)), Nothing],
  [Nothing, Nothing, Nothing], 
  [Nothing, (Just $ PlayerPiece Black (2, 1)), (Just $ PlayerPiece Black (2, 2))] ]

typesUnitTests :: TestTree
typesUnitTests = testGroup "Types Unit tests" [
  testCase "getOpppositeColor (Black)" $ getOppositeColor White @?= Black,
  testCase "getOpppositeColor (White)" $ getOppositeColor Black @?= White,
  testCase "getPiece (Nothing)" $ getPiece testEvenBoard (1, 1) @?= Nothing,
  testCase "getPiece (Just)" $ getPiece testEvenBoard (3, 1) @?= (Just $ PlayerPiece Black (3, 1)),
  testCase "getMirrorPiece (Nothing)" $ getMirrorPiece testEvenBoard (1, 1) @?= Nothing,
  testCase "getMirrorPiece (Just)" $ getMirrorPiece testEvenBoard (0, 1) @?= (Just $ PlayerPiece Black (3, 1)),
  testCase "areSquaresMirror" $ areSquaresMirror (Just $ PlayerPiece White (0, 1)) (Just $ PlayerPiece Black (3, 1)) @?= True,
  testCase "areRowsMirror" $ areRowsMirror testEvenBoard 0 3 @?= True,
  testCase "isMirrorBoard (even)" $ isMirrorBoard testEvenBoard @?= True,
  testCase "isMirrorBoard (odd)" $ isMirrorBoard testOddBoard @?= True,
  testCase "blackWon" $ blackWon blackWonBoard @?= True,
  testCase "whiteWon" $ whiteWon whiteWonBoard @?= True,
  testCase "countPieces" $ countPieces blackWonBoard 1 @?= (1, 0),
  testCase "isColorPresent" $ isColorPresent blackWonBoard (1, 0) White @?= True,
  testCase "isPiecePresent" $ isPiecePresent testEvenBoard (PlayerPiece White (0, 1)) (0, 1) @?= True,
  testCase "isPhalanxValid (fail - Piece)" $ isPhalanxValid (Piece $ PlayerPiece White (0, 1)) @?= False,
  testCase "isPhalanxValid (fail - length)" $ isPhalanxValid (Phalanx [(PlayerPiece White (0, 1))]) @?= False, 
  testCase "isPhalanxValid (fail - color)" $ isPhalanxValid (Phalanx [(PlayerPiece White (0, 1)), (PlayerPiece Black (0, 2))]) @?= False,
  testCase "isPhalanxValid (fail - line)" $ isPhalanxValid (Phalanx [(PlayerPiece White (0, 0)), (PlayerPiece White (0, 2))]) @?= False,
  testCase "isPhalanxValid (success)" $ isPhalanxValid (Phalanx [(PlayerPiece White (0, 1)), (PlayerPiece White (0, 2))]) @?= True,
  testCase "isValidMove (Piece fail - mirror board)" $ isValidMove whiteWonBoard (1, 1) (0, 1) (Piece $ PlayerPiece Black (1, 1)) @?= False,
  testCase "isValidMove (Piece fail - not present)" $ isValidMove testEvenBoard (1, 0) (1, 1) (Piece $ PlayerPiece White (0, 1)) @?= False,
  testCase "isValidMove (Piece fail - move by >1)" $ isValidMove testEvenBoard (0, 1) (2, 1) (Piece $ PlayerPiece White (0, 1)) @?= False,
  testCase "isValidMove (Piece fail - occupied)" $ isValidMove blackWonBoard (0, 1) (1, 1) (Piece $ PlayerPiece White (0, 1)) @?= False,
  testCase "isValidMove (Piece success)" $ isValidMove testEvenBoard (0, 1) (1, 1) (Piece $ PlayerPiece White (0, 1)) @?= True,
  testCase "isValidMove (Phalanx fail - mirror board)" $ isValidMove mirrorTestBoard (2, 1) (2, 0) (Phalanx [(PlayerPiece Black (2, 1)), (PlayerPiece Black (2, 2))]) @?= False,
  testCase "isValidMove (Phalanx fail - move by >length)" $ isValidMove testEvenBoard (0, 0) (0, 3) (Phalanx [(PlayerPiece White (0, 1)), (PlayerPiece White (0, 2))]) @?= False,
  testCase "isValidMove (Phalanx fail - wrong head)" $ isValidMove captureBoard (2, 1) (2, 3) (Phalanx [(PlayerPiece White (2, 1)), (PlayerPiece White (2, 0))]) @?= False,
  testCase "isValidMove (Phalanx fail - capture count)" $ isValidMove phalanxBoard (2, 1) (2, 3) (Phalanx [(PlayerPiece White (2, 1)), (PlayerPiece White (2, 0))]) @?= False,
  testCase "isValidMove (Phalanx fail - not present)" $ isValidMove testEvenBoard (1, 1) (2, 1) (Phalanx [(PlayerPiece White (0, 0)), (PlayerPiece White (0, 1))]) @?= False,
  testCase "isValidMove (Phalanx fail - through piece not in phalanx)" $ isValidMove whiteWonBoard (2, 1) (2, 2) (Phalanx [(PlayerPiece White (2, 1)), (PlayerPiece White (2, 2))]) @?= False,
  testCase "isValidMove (Phalanx success)]" $ isValidMove blackWonBoard (1, 0) (0, 0) (Phalanx [(PlayerPiece White (1, 0)), (PlayerPiece White (2, 0))]) @?= True,
  testCase "move Piece" $ move testEvenBoard (0, 0) (1, 0) (Piece $ PlayerPiece White (1, 0)) @?= testEvenBoard',
  testCase "move Phalanx without capture" $ move phalanxBoard (0, 2) (0, 0) (Phalanx [(PlayerPiece White (0, 2)), (PlayerPiece White (0, 3))]) @?= phalanxBoard',
  testCase "move Phalanx with capture" $ move phalanxBoard (2, 2) (2, 3) (Phalanx [(PlayerPiece White (2, 2)), (PlayerPiece White (2, 1)), (PlayerPiece White (2, 0))]) @?= captureBoard,
  testCase "createBoard" $ createBoard 3 3 @?= testOddBoard,
  testCase "goodToCoords (fail)" $ goodToCoords testOddBoard (4, 4) @?= False,
  testCase "goodToCoords (success)" $ goodToCoords testOddBoard (0, 0) @?= True ]

typesSmallCheckTests :: TestTree
typesSmallCheckTests =
  -- localOption (QuickCheckVerbose True) $
  testGroup "Types SmallCheck tests" 
  [ SC.testProperty "createBoard - row count" $ \x y -> (x >= 3 && y >= 2) SC.==> (length $ squares $ createBoard x y) == x,
    SC.testProperty "createBoard - column count" $ \x y -> (x >= 3 && y >= 2) SC.==> (length $ (squares $ createBoard x y) !! 0) == y,
    SC.testProperty "createBoard - white count" $ \x y -> (x >= 3 && y >= 2) SC.==> (mergePairs (+) (countPieces (createBoard x y) 0) (countPieces (createBoard x y) 1)) == if (x == 3 || x == 4) then (y, 0) else (y * 2, 0),
    SC.testProperty "createBoard - black count" $ \x y -> (x >= 3 && y >= 2) SC.==> (mergePairs (+) (countPieces (createBoard x y) $ x - 1) (countPieces (createBoard x y) $ x - 2)) == if (x == 3 || x == 4) then (0, y) else (0, y * 2) ]

typesQuickCheckTests :: TestTree
typesQuickCheckTests =
  -- localOption (QuickCheckVerbose True) $
  testGroup "Types QuickCheck tests" 
  [ QC.testProperty "createBoard - row count" $ \x y -> (x >= 3 && y >= 2) QC.==> (length $ squares $ createBoard x y) == x,
    QC.testProperty "createBoard - column count" $ \x y -> (x >= 3 && y >= 2) QC.==> (length $ (squares $ createBoard x y) !! 0) == y,
    QC.testProperty "createBoard - white count" $ \x y -> (x >= 3 && y >= 2) QC.==> (mergePairs (+) (countPieces (createBoard x y) 0) (countPieces (createBoard x y) 1)) == if (x == 3 || x == 4) then (y, 0) else (y * 2, 0),
    QC.testProperty "createBoard - black count" $ \x y -> (x >= 3 && y >= 2) QC.==> (mergePairs (+) (countPieces (createBoard x y) $ x - 1) (countPieces (createBoard x y) $ x - 2)) == if (x == 3 || x == 4) then (0, y) else (0, y * 2) ]


