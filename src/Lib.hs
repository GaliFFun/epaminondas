module Lib where


-- Поменять элемент в списке
replace :: (a -> a) -> Int -> [a] -> [a]
replace f 0 (x:xs) = (f x):xs
replace f i (x:xs) = x : replace f (i-1) xs
replace f i [] = []

-- Поменять элемент в списке списков
replace2D :: (a -> a) -> (Int, Int) -> [[a]] -> [[a]]
replace2D f (x, y) = replace (replace f y) x

-- Сливаем вторую пару в первую с помощью переданной функции
mergePairs :: (a -> a -> a) -> (a, a) -> (a, a) -> (a, a)
mergePairs f (a, b) (c, d) = (,) (f a c) (f b d) 

-- Заменяем Nothing на дефолтное значение
replaceWithDefault :: Maybe a -> a -> Maybe a
replaceWithDefault m def = 
  case m of
    Nothing -> Just def
    Just a -> Just a

-- Развертка Maybe с заменой на дефолтное значение в случае Nothing
getOrElse :: Maybe a -> a -> a
getOrElse (Just x) _ = x
getOrElse Nothing x = x

-- Находится ли значение не строго между двумя значениями
between :: (Ord a) => a -> a -> a -> Bool
between x left right = left <= x && x <= right
