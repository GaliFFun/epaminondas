{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api where

import Prelude hiding (lookup)
import Types
import Lib
import Data.Map
import Data.UUID
import Data.UUID.V4
import Data.Aeson
import Data.Aeson.Types
import GHC.Generics
import Control.Concurrent.STM.TVar (TVar, newTVar, readTVar, writeTVar)
import Control.Monad.IO.Class      (liftIO)
import Control.Monad.STM           (atomically)
import Servant.API
import Servant
import Control.Monad.Trans.Reader (ReaderT, ask, runReaderT)

-- Статус игры
data GameStatus = Starting | WhiteTurn | BlackTurn | WhiteWon | BlackWon deriving (Eq, Generic, ToJSON, FromJSON)

-- Тип игровой сессии - идентификаторы пользователей и игровая доска
data Game = Game {
  gameId :: UUID,
  whitePlayer :: Maybe String,
  blackPlayer :: Maybe String,
  gameBoard :: Board,
  status :: GameStatus
} deriving (Eq, Generic, ToJSON, FromJSON)

-- Действие перемещения
data GameAction = GameAction {
  fromPos :: Position,
  toPos :: Position,
  moved :: Movable
} deriving (Eq, Generic, ToJSON, FromJSON)

-- Игровой шаг
data GameStep = GameStep {
  game :: UUID,
  player :: String,
  action :: GameAction
} deriving (Eq, Generic, ToJSON, FromJSON)
   
-- Хранилище игр
data State = State {
  games :: TVar Games
}

-- Алиас для мапы UUID -> Игра
type Games = Map UUID Game

-- Тип для создания игры
data CreateGameDto = CreateGameDto {
  playerName :: String,
  xCoord :: Int,
  yCoord :: Int,
  isWhite :: Bool
} deriving (Eq, Generic, FromJSON)

-- Тип для присоединения к игре
data JoinGameDto = JoinGameDto {
  joiner :: String,
  joinedGame :: UUID
} deriving (Eq, Generic, FromJSON)

-- Тип для отмены игры
data CancelGameDto = CancelGameDto {
  initiator :: String,
  canceledGame :: UUID
} deriving (Eq, Generic, FromJSON)

-- Игровое API
type EpaminondasAPI = -- POST /game/ - Создать игру
                      "game" :> ReqBody '[JSON] CreateGameDto :> Post '[JSON] Game 
                      -- GET  /canJoin/:name - свободна ли игра такого игрока?
                 :<|> "canJoin" :> Capture "name" UUID :> Get '[JSON] Bool
                      -- PUT /join/ - присоединиться к игре такого игрока
                 :<|> "join" :> ReqBody '[JSON] JoinGameDto :> Put '[JSON] Game
                      -- PUT /validateMove/ - проверка корректности хода
                 :<|> "validateMove" :> ReqBody '[JSON] GameStep :> Put '[JSON] Bool
                      -- PUT /move/ - делаем игровой ход
                 :<|> "move" :> ReqBody '[JSON] GameStep :> Put '[JSON] Game
                      -- GET /game/:name - получаем информацию об игре, если она есть
                 :<|> "game" :> Capture "name" UUID :> Get '[JSON] (Maybe Game)
                      -- DELETE /game/ - отмена игры
                 :<|> "game" :> ReqBody '[JSON] CancelGameDto :> Delete '[JSON] Bool

-- Следующий ход
getNextTurn :: GameStatus -> GameStatus
getNextTurn WhiteTurn = BlackTurn
getNextTurn BlackTurn = WhiteTurn
getNextTurn Starting = WhiteTurn
getNextTurn _ = error "Game ended, no more moves for you"

-- Можно ли зайти в эту игру? Можно зайти когда статус "Стартует"
isGameAvailable :: UUID -> (Map UUID Game) -> Bool
isGameAvailable key map = 
  case lookup key map of
    Nothing -> False
    Just game -> status game == Starting

-- Пользователь заходит в игру, занимая незанятое место, игра начинается ходом белых, без проверок
joinExGame :: String -> Game -> Game
joinExGame ign (Game g w b bo s) = Game g (replaceWithDefault w ign) (replaceWithDefault b ign) bo WhiteTurn

-- Проверка корректности шага в игре
isValidStep :: GameStep -> Game -> Bool
isValidStep (GameStep gid pl (GameAction f t mo)) (Game gi w b bo s) = notEnded && validPlayer && validMove where
  -- Игра не окончена
  notEnded = s == WhiteTurn || s == BlackTurn
  -- Ходит игрок правильного цвета
  playerColor = if getOrElse w "" == pl then White else if getOrElse b "" == pl then Black else error "Player is not in this game"
  validPlayer = (playerColor == White && s == WhiteTurn) || (playerColor == Black && s == BlackTurn)
  -- Правильный ход по логике игры
  validMove = isValidMove bo f t mo

-- Делаем шаг в игре, меняем статус игры
makeGameStep :: GameStep -> Game -> Game
makeGameStep step@(GameStep gi p act@(GameAction f t m)) game@(Game g w b bo s) = Game g w b (move bo f t m) (getNextTurn s)

-- Проверка и модификация статуса игры
checkGameStatus :: Game -> Game
checkGameStatus game@(Game gi w b bo s) = 
  case s of
    WhiteTurn -> if whiteWon bo then Game gi w b bo WhiteWon else game
    BlackTurn -> if blackWon bo then Game gi w b bo BlackWon else game
    _ -> game

-- Отмена игры
canBeCanceled :: String -> UUID -> Game -> Bool
canBeCanceled ign gameId' (Game g w b bo s) = (s == Starting) && g == gameId' 

-- Игровой сервер
server :: ServerT EpaminondasAPI AppM
server = createAndAddGame :<|> canJoin :<|> joinGame :<|> validateMove :<|> makeStep :<|> getGameInfo :<|> cancelGame where
  -- Создание игры
  createAndAddGame :: CreateGameDto -> AppM Game
  createAndAddGame (CreateGameDto ign x y flag)  = do
    randUuid <- liftIO nextRandom
    -- Создаем новую игру
    let g = Game {gameId = randUuid, whitePlayer = if flag then Just ign else Nothing, blackPlayer = if flag then Nothing else Just ign, gameBoard = createBoard x y, status = Starting} 
    State{games = p} <- ask
    -- Кладем новую игру в мапу игр
    liftIO $ atomically $ readTVar p >>= writeTVar p . (insert (gameId g) g)
    return g
  -- Проверка, можно ли зайти в эту игру
  canJoin :: UUID -> AppM Bool
  canJoin key =  do
    State{games = p} <- ask
    fmap (\x -> isGameAvailable key x) $ liftIO $ atomically $ readTVar p
  -- Зайти в игру
  joinGame :: JoinGameDto -> AppM Game
  joinGame (JoinGameDto ign gameId') = do
    State{games = p} <- ask
    -- Добавляем игрока в игру
    liftIO $ atomically $ readTVar p >>= writeTVar p . (adjust (joinExGame ign) gameId')
    -- Возвращаем игру
    fmap (\x -> x ! gameId') $ liftIO $ atomically $ readTVar p
  -- Проверка корректности игрового хода
  validateMove :: GameStep -> AppM Bool
  validateMove step = do
    State{games = p} <- ask
    fmap (\x -> isValidStep step $ x ! (game step)) $ liftIO $ atomically $ readTVar p
  -- Игровой шаг
  makeStep :: GameStep -> AppM Game
  makeStep step = do
    State{games = p} <- ask  
    -- Делаем шаг игры
    liftIO $ atomically $ readTVar p >>= writeTVar p . (adjust (makeGameStep step) (game step))
    -- Возвращаем игру
    fmap (\x -> x ! (game step)) $ liftIO $ atomically $ readTVar p
  -- Информация об игре, которую создал игрок с таким ником
  getGameInfo :: UUID -> AppM (Maybe Game)
  getGameInfo key = do
    State{games = p} <- ask
    -- Получаем игру из мапы, проверяем статус игры
    liftIO $ atomically $ readTVar p >>= writeTVar p . (adjust checkGameStatus key)
    fmap (\x -> lookup key x) $ liftIO $ atomically $ readTVar p
  -- Отмена игры
  cancelGame :: CancelGameDto -> AppM Bool
  cancelGame (CancelGameDto ign gameId') = do
    State{games = p} <- ask
    -- Можно ли отменить игру?
    canceled <- fmap (\x -> canBeCanceled ign gameId' (x ! gameId')) $ liftIO $ atomically $ readTVar p
    -- Отменяем, если можно
    liftIO $ atomically $ readTVar p >>= writeTVar p . (if canceled then delete gameId' else id)
    return canceled

-- Магия с туториала
type AppM = ReaderT State Handler

nt :: State -> AppM a -> Handler a
nt s x = runReaderT x s

api :: Proxy EpaminondasAPI
api = Proxy

app :: State -> Application
app s = serve api $ hoistServer api (nt s) server

