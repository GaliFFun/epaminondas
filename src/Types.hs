{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}

module Types where

import Data.List
import Lib
import Data.Aeson
import Data.Aeson.Types
import GHC.Generics

-- Цвет игрока
data PlayerColor = White | Black deriving (Eq, Generic, ToJSON, FromJSON)

instance Show PlayerColor where
  show White = "W"
  show Black = "B"

-- Получает противоположный цвет
getOppositeColor :: PlayerColor -> PlayerColor
getOppositeColor White = Black
getOppositeColor Black = White

-- Позиция на доске - пара координат
type Position = (Int, Int)

-- Фишка игрока - имеет информацию о цвете и ее координатах
data PlayerPiece = PlayerPiece { color :: PlayerColor, position :: Position } deriving (Eq, Generic, ToJSON, FromJSON)

instance Show PlayerPiece where
  show (PlayerPiece color position) = show color

-- Клетка на доске - там может быть фишка, а может и нет
type BoardSquare = Maybe PlayerPiece

-- Показ клетки
showBoardSquare :: BoardSquare -> String
showBoardSquare sq = case sq of
  Nothing -> "x"
  Just a -> show a

-- Доска для игры - двухмерный массив клеток
data Board = Board { squares :: [[BoardSquare]] } deriving (Eq, Generic, ToJSON, FromJSON)

-- Создает доску X на Y, если число строк 3 - 4, то будет 1 ряд фишек, в противном случае 2 ряда
createBoard :: Int -> Int -> Board
createBoard x y = if validBoard then newBoard else (Board [[]]) where
  -- Доска верная, если больше 3 строк на ней или больше 2 столбцов
  validBoard = (x >= 3) && (y >= 2)
  isOnePieceRow = (x == 3) || (x == 4)
  -- Генерация пустом строки и n пустых строк
  emptyRow = take y $ repeat Nothing
  emptyRows n = emptyHelper n where
    emptyHelper 0 = []
    emptyHelper n = emptyRow : (emptyHelper $ n - 1)
  -- Берем y фишек переданного цвета на линии с данным номером
  createColorRow color x' = take y $ map (\x -> Just $ PlayerPiece color (x', x)) $ iterate (+ 1) 0
  -- Доска, на которой 1 линия фишек каждого цвета
  onePieceRowBoard = Board (createColorRow White 0 : (emptyRows $ x - 2) ++ [createColorRow Black (x - 1)])
  -- Доска, на которой 2 линии фишек каждого цвета
  twoPieceRowBoard = Board (createColorRow White 0 : createColorRow White 1 : (emptyRows $ x - 4) ++ [createColorRow Black (x - 2)] ++ [createColorRow Black (x - 1)]) 
  newBoard = if isOnePieceRow then onePieceRowBoard else twoPieceRowBoard

-- Показ доски
instance Show Board where
  show (Board []) = ""
  show (Board (x:xs)) = showHelper x ++ "\n" ++ show (Board xs) where
    showHelper [] = ""
    showHelper (y:ys) = showBoardSquare y ++ showHelper ys

-- Количество строк доски
getBoardRowCount :: Board -> Int
getBoardRowCount (Board board) = length board

-- То, что можно передвигать - либо фишка, либо фаланга
data Movable = Piece PlayerPiece | Phalanx [PlayerPiece] deriving (Eq, Generic, ToJSON, FromJSON)

-- Получаем клетку
getPiece :: Board -> Position -> BoardSquare
getPiece (Board board) (x, y) = board !! x !! y

-- Получаем "зеркальную клетку" - стояющую на строке, отстоящей от "домашней" на одинаковое кол-во, но при этом в одинаковой ячейке строки
getMirrorPiece :: Board -> Position -> BoardSquare
getMirrorPiece (Board board) (x, y) = getPiece (Board board) $ (,) (abs $ x - length board + 1)  y

-- Зеркальный ли набор фишек на доске?
isMirrorBoard :: Board -> Bool
-- Одна строка - зеркальная
isMirrorBoard (Board squares@[x]) = True
-- Проверка, зеркальные ли две строки
isMirrorBoard (Board squares@[x, y]) = areRowsMirror (Board squares) 0 (length squares - 1)
-- Рекурсивно проверяем, зеркальные ли первая и последняя строка
isMirrorBoard (Board squares) = areRowsMirror (Board squares) 0 (length squares - 1) && isMirrorBoard (Board $ cutFirstAndLastRows squares) where
  -- Режем первую и последнюю строки
  cutFirstAndLastRows [] = []
  cutFirstAndLastRows [x] = []
  cutFirstAndLastRows xs = tail $ init xs

-- Зеркальные ли строки?
areRowsMirror :: Board -> Int -> Int -> Bool
areRowsMirror (Board squares) x y  = mirrorHelper (squares !! x) (squares !! y) where
  mirrorHelper [] [] = True
  mirrorHelper (x:xs) (y:ys) = areSquaresMirror x y && mirrorHelper xs ys

-- Зеркальные ли клетки? Да, если на них стоят фишки разного цвета, или на обоих ничего не стоит
areSquaresMirror :: BoardSquare -> BoardSquare -> Bool
areSquaresMirror (Just (PlayerPiece c1 p1)) (Just (PlayerPiece c2 p2)) = (c1 /= c2)
areSquaresMirror Nothing Nothing = True
areSquaresMirror _ _ = False

-- Выиграли ли черные? (белые стартуют на 1 строке)
blackWon :: Board -> Bool
blackWon board = (fst countsW) < (snd countsB) where 
  countsB = countPieces board 0
  countsW = countPieces board $ (length $ squares board) - 1

-- Выиграли ли белые? (черные стартуют на последней строке)
whiteWon :: Board -> Bool
whiteWon board = (fst countsW) > (snd countsB) where 
  countsB = countPieces board 0
  countsW = countPieces board $ (length $ squares board) - 1

-- Считаем, сколько фишек каждого цвета на строке с таким номером
countPieces :: Board -> Int -> (Int, Int)
countPieces (Board squares) rowNumber = countHelper $ squares !! rowNumber where
  countHelper [] = (0, 0)
  countHelper (Nothing : xs) = countHelper xs
  countHelper (Just (PlayerPiece color position) : xs) = addColor (countHelper xs) color where
    -- Прибавляем 1 к нужному цвету
    addColor (w, b) color = case color of
      White -> (w + 1, b)
      Black -> (w, b + 1)

-- Корректная ли фаланга?
isPhalanxValid :: Movable -> Bool
isPhalanxValid movable = 
  case movable of
    -- Просто фишка - не фаланга
    Piece _ -> False
    -- Фаланга корректна, если там больше 1 фишки, она 1 цвета и находится она вдоль одной линии
    Phalanx xs -> checkLength && sameColor && sameLine where
      -- Фаланга состоит из более 1 фишки
      checkLength = length xs > 1
      -- Число белых фишек или равно длине фаланги, или равно 0 (черная фаланга)
      sameColor = (whiteCount == length xs) || (whiteCount == 0) where
        whiteCount = sum $ map (\(PlayerPiece color position) -> if color == White then 1 else 0) xs
      -- Фаланга находится вдоль одной линии
      -- Получаем разницу всех координат фаланги
      deltas = foldl (\x y -> mergePairs (-) x y) (headX, headY) tailPieces where
        getPosition (PlayerPiece color position) = position
        headX = (fst . getPosition . head) xs
        headY = (snd . getPosition . head) xs
        tailPieces = tail $ map getPosition xs
      -- Проверяем, что она находится по
      sameLine = case deltas of
        -- Горизонтали
        (a, 0) -> abs a == 1
        -- Вертикали
        (0, b) -> abs b == 1
        -- Диагонали + другие случаи
        (a, b) -> abs a == 1 && abs b == 1

-- Перемещаем что-то на доске (без проверки)
move :: Board -> Position -> Position -> Movable -> Board
move board from to movable = moveHelper board from to movable where
  moveHelper (Board board) (fromX, fromY) (toX, toY) movable = 
    case movable of
      -- Фишка - просто перемещаем фишку на доске и на старое место кладем Nothing
      Piece (PlayerPiece color position) -> Board $ replace2D (const Nothing) (fromX, fromY) $ replace2D (const $ Just $ PlayerPiece color (toX, toY)) (toX, toY) board
      Phalanx xs -> Board $ phalanxMoveHelper xs $ removePhalanx board where
        -- Рекурсивно перемещаем каждую часть фаланги
        phalanxMoveHelper [] board = board
        phalanxMoveHelper ((PlayerPiece color position):xs) board = replace2D (const $ Just (PlayerPiece color (toX', toY'))) (toX', toY') (phalanxMoveHelper xs replaced) where
          -- Убираем текущую фишку, чтобы переместить ее
          replaced = replace2D (const Nothing) position board
          -- Новые координаты для след фишки
          toX' = (toX - fromX) + fst position
          toY' = (toY - fromY) + snd position
        -- Убираем вражескую фалангу
        removePhalanx board = removeHelper (toX, toY) (length xs - 1) board where
          removeHelper (toX, toY) 0 board = board
          -- Если есть фишка такого цвета - убираем. Иначе конец фаланги
          removeHelper (toX, toY) n board = if goodCoords && enemyColorPresent then replace2D (const Nothing) (toX, toY) (removeHelper (toX', toY') (n - 1) board) else board where
            enemyColorPresent = isColorPresent (Board board) (toX, toY) (getOppositeColor $ color $ head xs)
            goodCoords = goodToCoords (Board board) (toX, toY)
            toX' = min (toX - fromX) 1 + toX
            toY' = min (toY - fromY) 1 + toY
          
-- Есть ли данная фишка на данной позиции?
isPiecePresent :: Board -> PlayerPiece -> Position -> Bool
isPiecePresent board piece position = 
  case getPiece board position of
    Nothing -> False
    Just piece' -> piece' == piece

-- Есть ли фишка с таким цветом на этой позиции?
isColorPresent :: Board -> Position -> PlayerColor -> Bool
isColorPresent board position color =
  case getPiece board position of
    Nothing -> False
    Just (PlayerPiece color' position') -> color == color'

-- Валидные ли координаты для перемещения?
goodToCoords :: Board -> Position -> Bool
goodToCoords (Board board) (x, y) = (between x 0 $ length board - 1) && (between y 0 $ (length $ board !! 0) - 1)

-- Правильный ли ход?
isValidMove :: Board -> Position -> Position -> Movable -> Bool
isValidMove board (fromX, fromY) (toX, toY) movable = (if (toX == 0 || toX == getBoardRowCount board - 1) then (not $ isMirrorBoard $ move board (fromX, fromY) (toX, toY) movable) else True) &&
  case movable of 
    -- Для отдельной клетки
    Piece (PlayerPiece color position) -> piecePlaced && isNeighbour && notOccupied where
      -- А стоит ли эта фишка вообще на этой позиции?
      piecePlaced = isPiecePresent board (PlayerPiece color position) (fromX, fromY)
      -- Соседняя ли клетка?
      isNeighbour = (isNear . abs $ fromX - toX) && (isNear . abs $ fromY - toY)
        where isNear a = elem a [0, 1]
      -- Есть ли кто-то на этой клетке?
      notOccupied = case getPiece board (toX, toY) of
        Nothing -> True
        Just _ -> False
    -- Считаем, что первый элемент - голова, все проверки выполняются относительно того, что голова начинает движение
    Phalanx pieces -> (isPhalanxValid $ Phalanx pieces) && checkPhalanxSquareMoveCount from to && captureCountValid && captureMoveCountValid && (phalanxMoveHelper from to pieces) where
      phalanxHead = head pieces
      -- Количество фишек в фаланге
      piecesCount = length pieces
      -- Фалангу можно двигать на 1 ... (длина_фаланги) клеток
      checkCount count = between (abs count) 1 piecesCount
      -- Проверяем, правильное ли число клеток для перемещения
      getMoveDeltas fromPos toPos = mergePairs (min) (1, 1) $ mergePairs (-) toPos fromPos
      checkPhalanxSquareMoveCount fromPos toPos = 
        case mergePairs (-) toPos fromPos of
          -- Движение по строкам
          (a, 0) -> checkCount a
          -- Движение по столбцам
          (0, b) -> checkCount b
          -- Движение по диагонали + учет остальных случаев
          (a, b) -> checkCount a && checkCount b
      -- Подсчет правильного числа клеток при захвате фаланги
      captureMoveCountValid = (toX, toY) == captureToPos where
        -- Находим позицию так: плюсуем from с умноженными поэлементно дельтами хода и результатом captureMoveCountHelper
        captureToPos = mergePairs (+) from $ mergePairs (*) (getMoveDeltas from to) (d, d) where
          from = (fromX, fromY)
          to = (toX, toY)
          d = captureMoveCountHelper (length pieces) (toX, toY)
        -- Считаем, сколько клеток мы можем продвинуться с учетом вражеской фаланги
        captureMoveCountHelper 0 (toX, toY) = 0
        captureMoveCountHelper n to = if goodCoords then (if (isColorPresent board to (getOppositeColor . color $ phalanxHead)) then 1 else 1 + (captureMoveCountHelper (n - 1) to')) else 0 where
          goodCoords = goodToCoords board to
          deltas = getMoveDeltas from to
          to' = mergePairs (-) to deltas
      -- Проверка на верное количество захваченных фишек (если такие есть)
      captureCountValid = (countLinePieces to piecesCount) < piecesCount where
        currentColor = color $ phalanxHead
        -- Считаем число захваченных фишек
        countLinePieces to 0 = 0
        countLinePieces to n = (if (isColorPresent board to (getOppositeColor currentColor)) then 1 + (countLinePieces to' (n - 1)) else 0) where
          deltas = getMoveDeltas from to
          to' = mergePairs (-) to deltas
      -- Позиция с которой идет движение
      from = (fromX, fromY)
      -- Позиция куда идет движение
      to = (toX, toY)
      -- Рекурсивная проверка движения фаланги
      phalanxMoveHelper fromPos toPos [] = True
      phalanxMoveHelper fromPos toPos ((PlayerPiece color position) : xs) = validPhalanxPieceMove && phalanxMoveHelper from' to' xs where
         -- Рассчет новых позиций для движения
         deltas = getMoveDeltas fromPos toPos
         from' = mergePairs (-) fromPos deltas
         to' = mergePairs (-) toPos deltas
         -- А стоит ли эта фишка вообще на этой позиции?
         piecePlaced = isPiecePresent board (PlayerPiece color position) fromPos
         -- На этом месте уже нет фишки такого же цвета, но фишка может быть из той же фаланги
         sameColorPieceNotPresent = if not $ isColorPresent board toPos color then elem (PlayerPiece color position) pieces else True
         validPhalanxPieceMove = piecePlaced && sameColorPieceNotPresent
